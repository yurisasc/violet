package advprog.example.bot.controller;

import advprog.example.bot.domain.UserSchedule;
import advprog.example.bot.service.SingleUserService;
import advprog.example.bot.service.UserScheduleService;

import com.linecorp.bot.model.action.DatetimePickerAction;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.GroupSource;
import com.linecorp.bot.model.event.source.RoomSource;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TemplateMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.template.ButtonsTemplate;
import com.linecorp.bot.model.message.template.CarouselColumn;
import com.linecorp.bot.model.message.template.CarouselTemplate;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@LineMessageHandler
public class Controller {

    final int IDLE = 0;
    final int ASK_TITLE = 1;
    final int ASK_DESC = 2;
    final int ASK_DATE = 3;
    final int ASK_TIME = 4;

    @Autowired
    private SingleUserService singleUserService;
    @Autowired
    private UserScheduleService userScheduleService;

    private String id = null;
    private String groupId = null;
    private boolean isUser = false;
    private int pointer = IDLE;
    private String title = null;
    private String desc = null;
    private String date = null;
    private String time = null;

    @EventMapping
    public Message handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        TextMessageContent message = event.getMessage();
        return handleTextContent(event.getSource(), message);
    }

    @EventMapping
    public Message handlePostbackEvent(PostbackEvent event) {
        String data = event.getPostbackContent().getData();
        System.out.println(data + "----------");
        if (data.endsWith("date") && pointer == ASK_DATE) {
            pointer = ASK_TIME;
            Map<String, String> param = event.getPostbackContent().getParams();
            date = param.get("date");

            String imageUrl = createUri("/static/buttons/1040.jpg");
            CarouselTemplate carouselTemplate = new CarouselTemplate(
                    Arrays.asList(new CarouselColumn(imageUrl, "Time Picker",
                                    "Masukkan waktu deadlinenya", Arrays.asList(
                            new DatetimePickerAction("Time",
                                    "action=sel&only=time",
                                    "time",
                                    "06:15",
                                    "23:59",
                                    "00:00"))
                            )
                    )
            );
            TemplateMessage templateMessage = new TemplateMessage("Masukkan tanggal", carouselTemplate);
            return templateMessage;
        }
        else if (data.endsWith("time") && pointer == ASK_TIME) {
            Map<String, String> param = event.getPostbackContent().getParams();
            time = param.get("time");

            if (isUser) {
                try {
                    userScheduleService.create(title, desc, date, time, id);
                    pointer = IDLE;
                    return new TextMessage("Jadwal sudah disimpan!");
                } catch (Exception e) {
                    e.printStackTrace();
                    return new TextMessage("Maaf, ada error. Coba ulangi lagi ya :)");
                }
            }
        }
        else if (data.startsWith("groupId")) {
            pointer = ASK_TITLE;
            groupId = data.replace("groupId=","");
            if (singleUserService.findById(groupId) == null) {
                System.out.println("KOSONG------------");
                singleUserService.create(id);
            }
            return new TextMessage("Apa nama jadwalnya?");
        }

        return new TextMessage("Unhandled Exception");
    }

    public Message handleTextContent(Source source, TextMessageContent message) {
        String commandText = message.getText();
        id = getId(source);
        isUser = isUser(source);

        if (commandText.startsWith("/new")) {
            if (isUser) {
                pointer = ASK_TITLE;
                if (singleUserService.findById(id) == null) {
                    System.out.println("KOSONG------------");
                    singleUserService.create(id);
                }
                return new TextMessage("Apa nama jadwalnya?");
            } else {
                groupId = id;
                String imageUrl = createUri("/static/buttons/1040.jpg");
                ButtonsTemplate buttonsTemplate = new ButtonsTemplate(
                        imageUrl,
                        "My button sample",
                        "Hello, my button",
                        Arrays.asList(
                                new PostbackAction("Create New Schedule", "groupId=" + groupId, "Create")
                        ));
                TemplateMessage templateMessage = new TemplateMessage("Button alt text", buttonsTemplate);
//                new CarouselColumn("hoge", "fuga", Arrays.asList(
//                        new PostbackAction("Create New Schedule", "groupId=" + groupId, "Create")));
                return null;
            }
        }

        else if (pointer == ASK_TITLE && !commandText.startsWith("/")) {
            pointer = ASK_DESC;
            title = commandText;
            return new TextMessage("Oke. Apa detail jadwalnya?");
        }

        else if (pointer == ASK_DESC && !commandText.startsWith("/")) {
            pointer = ASK_DATE;
            desc = commandText;

            String imageUrl = createUri("/static/buttons/1040.jpg");
            CarouselTemplate carouselTemplate = new CarouselTemplate(
                    Arrays.asList(new CarouselColumn(imageUrl, "Date Picker",
                            "Masukkan tanggalnya", Arrays.asList(
                            new DatetimePickerAction("Date",
                                    "action=sel&only=date",
                                    "date",
                                    "2017-06-18",
                                    "2100-12-31",
                                    "1900-01-01"))
                            )
                    )
            );
            TemplateMessage templateMessage = new TemplateMessage("Masukkan tanggal", carouselTemplate);
            return templateMessage;
        }

        else if (commandText.startsWith("/view")) {
            List<UserSchedule> scheduleList = userScheduleService.getScheduleById(id);
            StringBuilder jadwal = new StringBuilder();
            for (int i = 0; i < scheduleList.size(); i++) {
                jadwal.append((i+1)+". ");
                jadwal.append(scheduleList.get(i).getTitle() + ", ");
                jadwal.append(scheduleList.get(i).getDescription() + ", ");
                jadwal.append(scheduleList.get(i).getDue_date() + ", ");
                jadwal.append(scheduleList.get(i).getDue_time() + ".\n");
            }
            return new TextMessage(jadwal.toString());
        }

        else {
            return null;
        }
    }

    private static String createUri(String path) {
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(path).build()
                .toUriString();
    }

    public String getId(Source source) {
        if (source instanceof UserSource) {
            return source.getUserId();
        }
        else if (source instanceof GroupSource){
            return ((GroupSource) source).getGroupId();
        }
        else {
            return ((RoomSource) source).getRoomId();
        }
    }

    public boolean isUser(Source source) {
        if (source instanceof UserSource) {
            return true;
        }
        return false;
    }
}
