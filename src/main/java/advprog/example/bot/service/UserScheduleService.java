package advprog.example.bot.service;

import advprog.example.bot.domain.SingleUser;
import advprog.example.bot.domain.UserSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class UserScheduleService {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public UserSchedule findById(String id) {
        return em.find(UserSchedule.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<UserSchedule> getScheduleById(String userId) {
        return (List<UserSchedule>) em.createQuery("select u from UserSchedule u where user_id = ?1")
                .setParameter(1, userId)
                .getResultList();
    }

    @Transactional
    public void create(String title, String description, String date, String time, String id) throws ParseException { ;
        UserSchedule schedule = new UserSchedule();
        schedule.setTitle(title);
        schedule.setDescription(description);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        schedule.setDue_date(new Date(format.parse(date).getTime()));

        schedule.setDue_time(Time.valueOf(time+":00"));

        SingleUser user = em.getReference(SingleUser.class, id);
        schedule.setUser(user);

        em.persist(schedule);

    }

    public void delete(String id) {

    }
}
