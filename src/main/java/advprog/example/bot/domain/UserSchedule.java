package advprog.example.bot.domain;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.UUID;

@Entity
@Table(name = "user_schedule")
public class UserSchedule {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_schedule_id")
    UUID userScheduleId;

    @Column(name = "title")
    String title;

    @Column(name = "description")
    String description;

    @Column(name = "due_date")
    Date due_date;

    @Column(name = "due_time")
    Time due_time;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    SingleUser user;

    public UUID getUserScheduleId() {
        return userScheduleId;
    }

    public void setUserScheduleId(UUID userScheduleId) {
        this.userScheduleId = userScheduleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDue_date() {
        return due_date;
    }

    public void setDue_date(Date due_date) {
        this.due_date = due_date;
    }

    public Time getDue_time() {
        return due_time;
    }

    public void setDue_time(Time due_time) {
        this.due_time = due_time;
    }

    public SingleUser getUser() {
        return user;
    }

    public void setUser(SingleUser user) {
        this.user = user;
    }

}
