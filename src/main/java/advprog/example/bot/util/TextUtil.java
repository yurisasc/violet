package advprog.example.bot.util;

import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TextUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(TextUtil.class);
    private static final Pattern COMMAND = Pattern.compile("^/\\w+\\s");
    private static final String EMPTY = "";

    public static String getCommandArguments(String commandText) {
        LOGGER.debug(String.format("commandText: '%s'", commandText));
        String parsedArgs = commandText.replaceFirst(COMMAND.pattern(), EMPTY);

        LOGGER.debug(String.format("parsedArgs: '%s'", parsedArgs));
        return parsedArgs.trim();
    }
}
