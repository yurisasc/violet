package advprog.example.bot.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("A TextUtil")
class TextUtilTest {

    @Test
    @DisplayName("retrieves arguments of a command correctly")
    void testGetCommandArguments() {
        String input = "/echo Lorem Ipsum";

        assertEquals("Lorem Ipsum", TextUtil.getCommandArguments(input));
    }

    @Test
    @DisplayName("retrieves arguments of a command with underscore char correctly")
    void testGetCommandArgumentsWithUnderscores() {
        String input = "/a_command_b Lorem Ipsum";

        assertEquals("Lorem Ipsum", TextUtil.getCommandArguments(input));
    }
}