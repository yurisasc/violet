package advprog.example.bot.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "singleuser")
public class SingleUser {

    @Id
    @Column(name = "user_id")
    String userId;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Set<UserSchedule> schedules;

    public String getUserId() {
        return userId;
    }
    public void setUserId(String id) {
        this.userId = id;
    }

}
