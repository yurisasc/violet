package advprog.example.bot.service;

import advprog.example.bot.domain.SingleUser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Service
public class SingleUserService {

    @Autowired
    private EntityManager em;

    public SingleUser findById(String id) {
        return em.find(SingleUser.class, id);
    }

    public List<SingleUser> getAll() {
        return null;
    }

    @Transactional
    public void create(String id) {
        SingleUser user = new SingleUser();
        user.setUserId(id);
        em.persist(user);
    }

    public void delete(String id) {

    }
}
