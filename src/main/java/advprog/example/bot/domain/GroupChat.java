package advprog.example.bot.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "groupchat")
public class GroupChat {

    @Id
    @Column(name = "group_id")
    String groupId;

    public String getGroupId() {
        return groupId;
    }

}
